#include <stdio.h>      //printf(), scanf()
#include <stdlib.h>     //exit()
#include <sys/socket.h> //socket(), bind(), sendto(). recvfrom()
#include <netinet/in.h> //strucktura sockkaddr_in
#include <string.h>     //strcpy(), strcat()
#include <arpa/inet.h>  //inet_ntop(), inet_pton()
#include <stdbool.h>
#include "struktury.h"

#define PROTOCOL 158
#define SERWER_PORT 8888       // port serwera
#define SERWER_IP "127.0.0.1"  // adres serwera
#define CLIENT_IP "127.0.0.12" // adres klienta
#define ROZMIAR_BUFORA 65536   // maksymalna długośc wiadomości do serwera

#define clear() printf("\033[H\033[J")

int dodaj(int arr[], int size)
{
    int sum = 0;
    for (int i = 0; i < size; i++)
    {
        sum += arr[i];
    }
    return sum;
}
int odejmij(int arr[], int size)
{
    int dif = 0;
    for (int i = 0; i < size; i++)
    {
        dif -= arr[i];
    }
    return dif;
}
int pomnoz(int arr[], int size)
{
    int mul = 1;
    for (int i = 0; i < size; i++)
    {
        mul *= arr[i];
    }
    return mul;
}
int podziel(int arr[], int size)
{
    int div = 1;
    if (arr[9] == 0 || arr[8] == 0)
    {
        arr[9] = 1;
        arr[8] = 1;
    }
    else
    {
        div = arr[9] / arr[8];
    }
    return div;
}

int main(int argc, char *argv[])
{
    int deskryptor_gniazda;
    int rozmiar_danych = 0; // zmienna do kotrej wpisujemy wynikk dzialnia funkcji "sendto" i "recvfrom"
    int rozmiar_danych_raw = 0;
    int sockaddr_len = 0;
    struct sockaddr_in serwer;                                      // struktura opisująca serwer, ip, port itp.
    struct sockaddr_in client;                                      // struktura opisująca serwer, ip, port itp.
    socklen_t len = sizeof(serwer);                                 // długość struktury serwer
    unsigned char *bufor = (unsigned char *)malloc(ROZMIAR_BUFORA); // przechowywanie wiadomości od klienta
    unsigned char *bufor_klient = (unsigned char *)malloc(ROZMIAR_BUFORA);

    sockaddr_len = sizeof(serwer);
    printf("Aplikacja serwera\n");
    deskryptor_gniazda = socket(AF_INET, SOCK_RAW, PROTOCOL); // stworzenie gniadza z definicją protokołu, SOCK_DGRAM = UDP, SOCK_STREAM = TCP
    if (deskryptor_gniazda < 0)
    {
        perror("Gniazdo nie zostao utworzone...");
        exit(-1);
    }

    serwer.sin_addr.s_addr = inet_addr(SERWER_IP);
    serwer.sin_family = AF_INET;          // Wersja IP: IPv4
    serwer.sin_port = htons(SERWER_PORT); // przetłumaczenie portu z formy liczbowej na bitową

    if (bind(deskryptor_gniazda, (struct sockaddr *)&serwer, sockaddr_len) < 0) // połączenie giazda z adresem IP oraz portem
    {
        printf("Gniazdo adresu nie zostao dowiazane...\n");
        exit(-1);
    }
    printf(" Utworzono prawidowo aplikacje klienta: \n\n");

    // Nieskończona pętla
    while (1)
    {
        printf("\nSerwer nasluchiwanie ...\n");
        // odebranie wiadomosci od klienta
        rozmiar_danych = recvfrom(deskryptor_gniazda, bufor, ROZMIAR_BUFORA, 0, (struct sockaddr *)&serwer, (socklen_t *)&sockaddr_len);
        if (rozmiar_danych < 0)
        {
            printf("[ERROR...]\n");
            exit(-1);
        }

        rozmiar_danych_raw = rozmiar_danych - 20;
        memcpy(bufor_klient, &bufor[20], rozmiar_danych_raw);

        // Przetrzymywanie ip klienta
        char client_ip[128];
        /* inet_ntop() - Tłumaczenie adresu IP z sieciowego na tekstowy
         * nthos() - Tłumaczenie portu z formatu sieciowego na liczbowy
         */

        printf("|IP Adres Klienta: %s | Port: %d|\n", inet_ntop(AF_INET, &serwer.sin_addr, client_ip, sizeof(client_ip)), ntohs(serwer.sin_port));
        printf("[!]Odebrana wiadomosc\n");
        printf("Rozmiar danych: %d\n", rozmiar_danych);

        int wersja_danych = bufor_klient[0];
        int kod_danych = bufor_klient[1];
        int typ_danych = bufor_klient[2];
        int rozmiar_danych = bufor_klient[3];
        int data_length = bufor_klient[4];

        printf("\tWersja danych: %d\n", wersja_danych);
        printf("\tKod operacji: %d\n", kod_danych);
        printf("\tTyp danych: %d\n", typ_danych);
        printf("\tRozmiar danych: %d\n", rozmiar_danych);
        printf("\tDlugosc danych: %d\n", data_length);

        if (typ_danych == 1)
        {
            // 6 BAJTOW
            printf("Adress MAC: ");
            for (int i = 0; i < 6; i++)
                printf("%02X:", bufor_klient[data_length + i]);

            bufor_klient[2] = 0x04;
            bufor_klient[1] = 0x06;
        }
        else if (typ_danych == 2)
        {
            printf("Adress IP: ");
            for (int i = 0; i < 4; i++)
                printf("%i.", bufor_klient[data_length + i]);

            bufor_klient[2] = 0x04;
            bufor_klient[1] = 0x06;
        }
        else if (typ_danych == 3)
        {
            int liczby[10];
            memcpy(&liczby, &bufor_klient[data_length], rozmiar_danych);

            int operacja;
            int wynik = 0, a, b, c;
            int rozmiar = sizeof(liczby) / sizeof(liczby[0]);
            operacja = bufor_klient[1];
            printf("Operacja = %d\n", operacja);
            if (operacja == 1 || operacja == 2 || operacja == 3 || operacja == 4 || operacja == 5)
            {

                switch (operacja)
                {
                case 1:
                    break;
                case 2:
                    wynik = dodaj(liczby, rozmiar);
                    printf("[+] DODAWANIE: %i", wynik);
                    break;
                case 3:
                    wynik = odejmij(liczby, rozmiar);
                    printf("[-] ODEJMOWANIE: %i", wynik);
                    break;
                case 4:
                    wynik = pomnoz(liczby, rozmiar);
                    printf("[*] MNOZENIE: %i", wynik);
                    break;
                case 5:
                    wynik = podziel(liczby, rozmiar);
                    printf("[/] DZIELENIE: %i", wynik);
                    break;
                default:
                    break;
                }
                liczby[0] = wynik; // Przepisanie wyniku operacji do tablicy od poczatku
            }

            bufor_klient[2] = 0x04;
            bufor_klient[1] = 0x07;
            bufor_klient[3] = data_length / 2;
            memcpy(&bufor_klient[data_length], &liczby, 40);
        }

        client.sin_addr.s_addr = inet_addr(client_ip); // ustaw adres ip klienta do wyslanai wiadomosci
        client.sin_family = AF_INET;                   // IPv4
        client.sin_port = htons(SERWER_PORT);

        // Wysłanie wiadomości do klienta
        if (sendto(deskryptor_gniazda, bufor_klient, rozmiar_danych_raw, 0, (struct sockaddr *)&client, sockaddr_len) < 0)
        {
            printf("[ERROR...]\n");
            exit(-1);
        }

        printf("\n");
    }

    // Zamykam gniazdo
    shutdown(deskryptor_gniazda, SHUT_RDWR);

    return 0;
}
