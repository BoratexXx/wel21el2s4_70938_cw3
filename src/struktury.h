#ifndef STRUKTURY_H
#define STRUKTURY_H

struct Opcje {
    unsigned char kod_opcji;
    unsigned char dlugosc;
    unsigned char dane[20];
};

struct Naglowek {

    unsigned char wersja;
    unsigned char kod;
    unsigned char typDanych;
    unsigned char dlugosc;
    unsigned char rozmiarDanych;
    struct Opcje opcje;
};

struct PDU {
    struct Naglowek naglowek;
    unsigned char dane[20];
};

#endif

