#include <stdio.h>      //printf(), scanf()
#include <stdlib.h>     //exit()
#include <linux/if.h>   // MAC Adress
#include <sys/socket.h> //socket(), bind(), sendto(). recvfrom()
#include <netinet/in.h> //strucktura sockkaddr_in
#include <string.h>     //strcpy(), strcat()
#include <arpa/inet.h>  //inet_ntop(), inet_pton()
#include <netinet/in.h> //strucktura sockkaddr_in
#include <stdbool.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include "struktury.h" //Wczytanie jak maja wygladac nasze ramki DPU

#define PROTOCOL 158
#define SERWER_PORT 8888       // port serwera
#define SERWER_IP "127.0.0.1"  // adres serwera
#define CLIENT_IP "127.0.0.12" // adres klienta
#define ROZMIAR_BUFORA 65536   // maksymalna długośc wiadomości do serwera
#define wyczysc_ekran() printf("\033[H\033[J")

int main()
{
    int deskryptor_gniazda; // numer gnazda
    int rozmiar_danych = 0;
    int rozmiar_danych_raw = 0;
    int sockaddr_len = 0;
    struct sockaddr_in serwer;                                            // struktura opisująca serwer do którego będziemy wysyłac dane, ip, port itp.
    struct sockaddr_in client;                                            // struktura opisująca client do którego będziemy wysyłac dane, ip, port itp.
    unsigned char *bufor = (unsigned char *)malloc(ROZMIAR_BUFORA);       // przechowywanie wiadomości od klienta
    unsigned char *serwer_buf = (unsigned char *)malloc(ROZMIAR_BUFORA);  // przechowywanie odpowiedzi od serwera która idzie do klienta
    unsigned char *dane_serwer = (unsigned char *)malloc(ROZMIAR_BUFORA); // // przechowywanie danych od serwera

    sockaddr_len = sizeof(serwer); // rozmiar struktury serwera
    printf("Aplikacja klienta\n");
    deskryptor_gniazda = socket(AF_INET, SOCK_RAW, PROTOCOL); // stworzenie gniadza z definicją protokołu
    if (deskryptor_gniazda < 0)
    {
        perror("Gniazdo nie zostao utworzone...");
        exit(-1);
    }

    client.sin_addr.s_addr = inet_addr(CLIENT_IP); // zdefiniowanie adresu IP gniazda sieciowego klienta
    client.sin_family = AF_INET;                   // zdefiniowanie rodziny protokololow
    client.sin_port = htons(SERWER_PORT);          // zdefiniowanie numeru portu

    if (bind(deskryptor_gniazda, (struct sockaddr *)&client, sizeof(client)) < 0) // przypisanie ustawionego wyzej adresu do gniazda
    {
        printf("Gniazdo adresu nie zostao dowiazane...\n");
        exit(-1);
    }

    serwer.sin_family = AF_INET;          // Wersja IP: IPv4
    serwer.sin_port = htons(SERWER_PORT); // przetłumaczenie portu z formy liczbowej na bitową

    if (inet_pton(AF_INET, SERWER_IP, &serwer.sin_addr) <= 0) // przetłumaczenie adresu z formy tekstowe do reprezentacji sieciowej
    {
        perror("[ERROR...] Bledna translacja adresu");
        exit(-1);
    }

    printf("Aplikacje klienta prawidlowo skonfigurowana : \n\n");
    // Przygotowanie i wyslanie wiadomosci
    // Wyslanie wiadomosci

    // zmienne pomocnicze - naglowek
    int kodWiadomosci = 1;
    int typWiadomosci = 0;
    int data_length = 0;
    unsigned char data[40];

    // zmienne pomocnicze - typ wiadomosci do serwera
    struct ifreq ifr;
    struct ifconf ifc;
    char buf[1024];
    int result = 0;
    int liczby[10];
    int operacja = 0;

    sleep(1); // 1 sekunda

    while (1)
    {
        //wyczysc_ekran();
        // Wprowadzanie przez uzytkownika rodzaju wysylanej wiadomosci
    skok:
        printf("Prosze wybrac typ operacji jaka chcesz wykonac: \n");
        printf("\t1 -> W polu danych adres przygotowac i przeslac MAC\n");
        printf("\t2 -> W polu opcji  adres przygotowac i przeslac  IP\n");
        printf("\t3 -> W polu danych przygotowac i przeslac 10 cyfr\n");
        printf("\n\n>:");
        scanf("%d", &typWiadomosci); // pobranie wyboru uzytkownika
        if (typWiadomosci == 1 || typWiadomosci == 2 || typWiadomosci == 3)
        {
            printf("kod operacji zosta wybrany %d\n", typWiadomosci);
        }
        else
        {
            printf("Wprowadzono zostal niepoprawny kod operacji !\n");
            goto skok;
        }

        printf("Wybrano: %02X\n", typWiadomosci);

        switch (typWiadomosci)
        {
        case 1:
            /* code */
            /*Wyslanie adresu MAC*/

            ifc.ifc_len = sizeof(buf);
            ifc.ifc_buf = buf;
            if (ioctl(deskryptor_gniazda, SIOCGIFCONF, &ifc) == -1) // odwolanie sie do warstwy fizycznej (ADRES MAC)
            {                                                       /* handle error */
                perror("[ERROR...]");
            }

            struct ifreq *it = ifc.ifc_req;
            const struct ifreq *const end = it + (ifc.ifc_len / sizeof(struct ifreq));

            // odczytanie MAC
            for (; it != end; ++it)
            {
                strcpy(ifr.ifr_name, it->ifr_name);
                if (ioctl(deskryptor_gniazda, SIOCGIFFLAGS, &ifr) == 0)
                {
                    if (!(ifr.ifr_flags & IFF_LOOPBACK))
                    { // don't count loopback
                        if (ioctl(deskryptor_gniazda, SIOCGIFHWADDR, &ifr) == 0)
                        {
                            break;
                        }
                    }
                }
            }
            //przechowuje MAC
            unsigned char *mac;
            mac = (unsigned char *)ifr.ifr_hwaddr.sa_data;
            printf("\tAdres MAC interfejsu %s: %.2x:%.2x:%.2x:%.2x:%.2x:%.2x\n",
                   ifr.ifr_name, mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
            memcpy(data, mac, 6);
            data_length = 6;

            break;
        case 2:
            /* code */
            //Adres IP klienta
            data[0] = 192;
            data[1] = 168;
            data[2] = 0;
            data[3] = 12;

            char client_ip[4];

            sscanf(client_ip, "%i.%i.%i.%i", &data[0], &data[1], &data[2], &data[3]);

            data_length = 4;
            break;
        case 3:
            /* code */
            // Liczby

            printf("Wpisz liczby do serwera: \n");
            for (int i = 0; i < 10; i++)
            {
                printf("L%d ->", i);
                scanf("%d", &liczby[i]);
            }
            sleep(1);
            printf("Podaj rodzaj operacji: \n |\t1 - brak operacji\n |\t2 - dodawanie\n |\t3 - odejmowanie\n |\t4 - mnozenie\n |\t5 - dzielenie\n");
            scanf("%d", &operacja);
            while (operacja < 1 || operacja > 5)
            {
                scanf("%d", &operacja);
            }

            kodWiadomosci = operacja;
            data_length = sizeof(liczby);
            memcpy(data, &liczby, data_length);
            break;
        default:
            break;
        }

        struct PDU wiadomosc;
        wiadomosc.naglowek.wersja = 1;
        wiadomosc.naglowek.kod = kodWiadomosci;
        wiadomosc.naglowek.typDanych = typWiadomosci;
        wiadomosc.naglowek.rozmiarDanych = data_length;

        memcpy(wiadomosc.dane, &data, wiadomosc.naglowek.rozmiarDanych);

        wiadomosc.naglowek.opcje.kod_opcji = wiadomosc.naglowek.kod;
        wiadomosc.naglowek.opcje.dlugosc = wiadomosc.naglowek.rozmiarDanych;
        memcpy(wiadomosc.naglowek.opcje.dane, &wiadomosc.dane, wiadomosc.naglowek.opcje.dlugosc);

        wiadomosc.naglowek.dlugosc = 7 + wiadomosc.naglowek.opcje.dlugosc;

        memcpy(&bufor[0], &wiadomosc.naglowek.wersja, 1);
        memcpy(&bufor[1], &wiadomosc.naglowek.kod, 1);
        memcpy(&bufor[2], &wiadomosc.naglowek.typDanych, 1);
        memcpy(&bufor[3], &wiadomosc.naglowek.rozmiarDanych, 1);
        memcpy(&bufor[4], &wiadomosc.naglowek.dlugosc, 1);
        memcpy(&bufor[5], &wiadomosc.naglowek.opcje.kod_opcji, 1);
        memcpy(&bufor[6], &wiadomosc.naglowek.opcje.dlugosc, 1);
        memcpy(&bufor[7], &wiadomosc.naglowek.opcje.dane, wiadomosc.naglowek.opcje.dlugosc);
        memcpy(&bufor[7 + wiadomosc.naglowek.opcje.dlugosc], &wiadomosc.dane, wiadomosc.naglowek.rozmiarDanych);

        int buforLen = 7 + wiadomosc.naglowek.opcje.dlugosc + wiadomosc.naglowek.rozmiarDanych;//zeby sie nie nadpisywaly

        printf("\nZawartosc informacyji (hex)\n");
        for (int i = 0; i < buforLen; i++)
        {
            if (i % 8 == 0)
                printf(" ");
            printf("%02X ", bufor[i]);
        }
        printf("\n");

        do
        {
            // Wysłanie wiadomości do serwera
            printf("Wyslanie pakietow do serwera\n");
            if (sendto(deskryptor_gniazda, bufor, buforLen, 0, (struct sockaddr *)&serwer, sockaddr_len) < 0)
            {
                printf("[ERROR...] sendto()\n");
                exit(-1);
            }
            printf("\n");

            // Odebranie wiadomosci od serwera (response)
            printf("Odbieranie wiadomosc od serwera\n");
            rozmiar_danych = recvfrom(deskryptor_gniazda, serwer_buf, ROZMIAR_BUFORA, 0, (struct sockaddr *)&serwer, (socklen_t *)&sockaddr_len);
            if (rozmiar_danych < 0)
            {
                printf("[ERROR...]recvfrom()\n");
                exit(-1);
            }

            rozmiar_danych_raw = rozmiar_danych - 20;
            memcpy(dane_serwer, &serwer_buf[20], rozmiar_danych_raw);

            printf("Odpowiedz serwera\n");

            int wersja_danych = dane_serwer[0];
            int kod_danych = dane_serwer[1];
            int typ_danych = dane_serwer[2];
            int rozmiar_danych = dane_serwer[3];
            int data_length = dane_serwer[4];
            int kod_opcji = dane_serwer[5];
            int rozmiar_opcji = dane_serwer[6];

            printf("Wersja: %d\n", wersja_danych);
            printf("Kod: %d\n", kod_danych);
            printf("Typ: %d\n", typ_danych);
            printf("Rozmiar: %d\n", rozmiar_danych);
            printf("Dlugosc: %d\n", data_length);
            printf("Kod opcji: %d\n", kod_opcji);
            printf("Dlugosc opcji: %d\n", rozmiar_opcji);
            printf("\n");

            if (kod_danych == 0x07 && typ_danych == 0x04)
            {
                int llb[10];
                memset(llb,0,10);
                memcpy(&llb, &dane_serwer[data_length], rozmiar_danych);
                printf("[SERWER...] wynik = %d\n", llb[0]);
            }

            sleep(4);

        } while (dane_serwer[2] != 0x04);

        sleep(2);
    }
    printf("\n");

    // Zamknięcie gniazda
    shutdown(deskryptor_gniazda, SHUT_RDWR);

    return 0;
}
